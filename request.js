
var reportData = {
	start: '20170724',
	logging: {
		interval: 'w',
		format: 'pdf'
	},
	owner: {
		company: 'Zalencentrum De Molen', 
		contact:'J.Helmond',
		title:'Mr',
		location:'Kleine zaal',
		street:'Hofplein 23',
		zipcode:'1234AB',
		city:'Utrecht',
		country:'The Netherlands',
		phone:'088-19283281',
		website:'somebv.nl',
		email:'jansen@somebv.nl'
	},
	installer:
	{
		name: "Super sound events",
		street: "De Paal 37",
		zip: "1351 JG",
		city: "Almere",
		country: "the Netherlands",
		phone: "+31365472222",
		fax: "--",
		website: "www.supersound.nl",
		email: "info@supersound.nl",
		remark: "standaard installateur"
	},
	device:{
		serial_number: '123456',
		calibration_date: '20170616'
	}
}

var sendReport = function(){
	var success = function(data){
		document.getElementById('send-response').innerHTML = data;
	}

	var error = function(e){
		document.getElementById('send-response').innerHTML = 'Error!<br>Status: ' + e.status;
	}
	reportData.logging.interval = document.getElementById('interval').selectedOptions[0].value;
	reportData.logging.format = document.getElementById('format').selectedOptions[0].value;
	var data = new FormData($('#my-form')[0]);
	var report = $.toJSON(reportData);

	data.append('report',encodeURI(report)); 
	document.getElementById('send-response').innerHTML = 'Waiting for response...';
	$.ajax({
		type:'post',
		url:'http://dateq.loc/spl6_report.php',	
		success: success,
		error: error,
		data: data,
		cache: false,
    	contentType: false,
    	processData: false
	});
}

document.getElementById('send-report').addEventListener('click',sendReport);